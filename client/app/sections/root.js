/// <reference path="../../../typings/client.d.ts" />
'use strict';
var config = require("../config");
var angular = require("angular");
angular.module(config.appName).controller(config.sections.root, RootController);
RootController.$inject = [config.common.srv.utils, config.common.srv.tree, config.common.srv.treeCache, "$q"];
function RootController(utilsService, treeService, treeCacheService, $q) {
    var vm = this;
    vm.isEditing = false;
    vm.editCache = null;
    vm.tree = undefined;
    vm.treeCache = undefined;
    vm.selectedDatabaseItem = null;
    vm.selectedCacheItem = null;
    vm.selectDatabaseItem = selectDatabaseItem;
    vm.selectCacheItem = selectCacheItem;
    vm.loadSelectedItem = loadSelectedItem;
    vm.addAfterItem = addAfterItem;
    vm.removeItem = removeItem;
    vm.resetAll = resetAll;
    vm.applyChanges = applyChanges;
    vm.enableEditing = enableEditing;
    vm.saveEditing = saveEditing;
    vm.cancelEditing = cancelEditing;
    activate();
    function activate() {
        treeService.getTree().then(function (result) {
            vm.tree = utilsService.nodesToTree(result);
        });
        vm.treeCache = utilsService.nodesToTree(treeCacheService.getTree());
    }
    function loadSelectedItem() {
        treeCacheService.getNode(vm.selectedDatabaseItem.id).then(function () {
            vm.selectedDatabaseItem = null;
            vm.treeCache = utilsService.nodesToTree(treeCacheService.getTree());
        });
    }
    function addAfterItem() {
        if (!vm.selectedCacheItem.deleted) {
            treeCacheService.createNode(vm.selectedCacheItem.id).then(function () {
                vm.selectedCacheItem = null;
                vm.treeCache = utilsService.nodesToTree(treeCacheService.getTree());
            });
        }
    }
    function applyChanges() {
        treeCacheService.saveChanges().then(function () {
            treeService.getTree().then(function (result) {
                vm.tree = utilsService.nodesToTree(result);
            });
            vm.treeCache = utilsService.nodesToTree(treeCacheService.getTree());
        });
    }
    function resetAll() {
        vm.isEditing = false;
        $q.all([treeCacheService.resetCache(), treeService.resetTree()]).then(function () {
            activate();
        });
    }
    function enableEditing() {
        if (!vm.selectedCacheItem.deleted) {
            vm.isEditing = true;
            vm.editCache = vm.selectedCacheItem.value;
        }
    }
    function saveEditing() {
        vm.isEditing = false;
        vm.selectedCacheItem.value = vm.editCache;
        vm.selectedCacheItem.modified = true;
        treeCacheService.putNode(utilsService.treeNodeToLocalNode(vm.selectedCacheItem));
    }
    function cancelEditing() {
        vm.isEditing = false;
        vm.editCache = null;
    }
    function selectCacheItem(node) {
        if (!vm.isEditing) {
            vm.selectedCacheItem = node;
        }
    }
    function selectDatabaseItem(node) {
        if (!vm.isEditing) {
            vm.selectedDatabaseItem = node;
        }
    }
    function removeItem() {
        if (!vm.selectedCacheItem.deleted) {
            vm.selectedCacheItem.deleted = true;
            vm.selectedCacheItem.modified = true;
            treeCacheService.putNode(utilsService.treeNodeToLocalNode(vm.selectedCacheItem));
            if (vm.selectedCacheItem.children) {
                for (var id in vm.selectedCacheItem.children) {
                    removeSubItem(vm.selectedCacheItem.children[id]);
                }
            }
        }
    }
    function removeSubItem(item) {
        if (item) {
            item.deleted = true;
            treeCacheService.putNode(utilsService.treeNodeToLocalNode(item));
            if (item.children) {
                for (var id in item.children) {
                    removeSubItem(item.children[id]);
                }
            }
        }
    }
}
module.exports = RootController;
//# sourceMappingURL=root.js.map