/**
 * Created by DmitrijTrifonov on 03/09/2015.
 */
import config = require("../../config");
import schemas = require("../schemas");
import angular = require("angular");

angular.module(config.appName).factory(config.common.srv.tree, getTreeService);

getTreeService.$inject = ["$http","$timeout", "$q","localStorageService"];

function getTreeService($http: ng.IHttpService,$timeout: ng.ITimeoutService, $q: ng.IQService,localStorageService: ng.local.storage.ILocalStorageService) {
    return new TreeService($http,$timeout, $q, localStorageService);
}

class TreeService implements ITreeService {

    private nodesCount:number = 0;
    private useRemoteApi:boolean = true;

    constructor(private $http:ng.IHttpService,
                private $timeout: ng.ITimeoutService,
                private $q:ng.IQService,
                private localStorageService: ng.local.storage.ILocalStorageService) {

        if (!this.useRemoteApi) {
            if (!localStorageService.get(config.common.storage.treeCounter)) {
                localStorageService.set(config.common.storage.treeCounter, this.nodesCount);
            } else {
                this.nodesCount = localStorageService.get<number>(config.common.storage.treeCounter);
            }

            if (!localStorageService.get(config.common.storage.tree)) {
                this.resetTree();
            }
        }
    }

    private getNextCounter() :number {
        this.nodesCount += 1;
        this.localStorageService.set(config.common.storage.treeCounter,this.nodesCount);
        return this.nodesCount;
    }

    private createTree(): ng.IPromise<boolean> {
        var deferred:ng.IDeferred<boolean> = this.$q.defer();

        this.createChildNode(null,0,4,deferred);
        deferred.notify();
        return deferred.promise;
    }
    private createChildNode(id:string,level:number,maxLevel:number,deferred: ng.IDeferred<boolean>) {
        var newId:string = this.getNextCounter().toString();
        var newNode: schemas.INode = {
            id: newId,
            parent: id,
            value: "Node "+newId,
            deleted: false
        };

        var nodes = this.localStorageService.get<schemas.INodes>(config.common.storage.tree);
        nodes[newNode.id] = newNode;
        this.localStorageService.set(config.common.storage.tree,nodes);
        if (level < maxLevel) {
            this.createChildNode(newNode.id,level+1,maxLevel,deferred);
            this.createChildNode(newNode.id,level+1,maxLevel,deferred);
        } else {
            return deferred.resolve(true);
        }
    }

    private deleteAllChildren(parentId:string,
                              tree: schemas.INodes,
                              modifiedNodes:schemas.INodes,
                              oldNodesIdsMap: {[newId:string]:string},
                              modified:schemas.INodes,
                              notChanged:schemas.INodes) {
        for (var id in tree) {
            if (tree[id].parent == parentId) {
                tree[id].deleted = true;

                if (oldNodesIdsMap && oldNodesIdsMap[id] && modifiedNodes && modifiedNodes[oldNodesIdsMap[id]]) {
                    modifiedNodes[oldNodesIdsMap[id]].deleted = true;
                } else if (modified && modified[id]) {
                    modified[id].deleted = true;
                    modifiedNodes[id] = modified[id];
                } else if (notChanged && notChanged[id]) {
                    notChanged[id].deleted = true;
                    modifiedNodes[id] = notChanged[id];
                }

                this.deleteAllChildren(id,tree,modifiedNodes,oldNodesIdsMap,modified,notChanged);
            }
        }
    }

    resetTree(): ng.IPromise<boolean> {
        var http = this.$http;
        if (this.useRemoteApi) {
            return http.post(config.apiUrl+"reset?format=json").then((callback) => true);
        } else {

            this.nodesCount = 0;
            var nodes:schemas.INodes = {};
            this.localStorageService.set(config.common.storage.treeCounter, this.nodesCount);
            this.localStorageService.set(config.common.storage.tree, nodes);

            return this.createTree();
        }
    }

    getTree(): ng.IPromise<schemas.INodes> {
        var http = this.$http;
        if (this.useRemoteApi) {
            return http.get(config.apiUrl+"nodes?format=json").then((callback) => callback.data);
        } else {
            var deferred = this.$q.defer();

            this.$timeout(() => {
                deferred.resolve(this.localStorageService.get(config.common.storage.tree));

            }, 300);

            return deferred.promise;
        }
    }

    getNode(id: string): ng.IPromise<schemas.INode> {
        var http = this.$http;
        if (this.useRemoteApi) {
            return http.get(config.apiUrl+"node/"+id+"?format=json").then((callback) => callback.data);
        } else {
            var deferred = this.$q.defer();

            this.$timeout(() => {
                deferred.resolve(this.localStorageService.get(config.common.storage.tree)[id]);

            }, 300);

            return deferred.promise;
        }
    }

    saveChanges(created: schemas.INodes,modified: schemas.INodes,deleted: schemas.INodes, notChanged: schemas.INodes): ng.IPromise<schemas.INodes> {
        var http = this.$http;
        if (this.useRemoteApi) {
            return http.post(config.apiUrl+"apply?format=json",{created:created,modified:modified,deleted:deleted,notChanged:notChanged}).then((callback) => callback.data);
        } else {

            var deferred = this.$q.defer();

            this.$timeout(() => {
                var nodesTree = this.localStorageService.get<schemas.INodes>(config.common.storage.tree);
                var newNodesIdsMap:{[oldId:string]:string} = {};
                var oldNodesIdsMap:{[newId:string]:string} = {};
                var modifiedNodes:schemas.INodes = {};

                if (created) {
                    for (var nodeId in created) {
                        var newNodeId = this.getNextCounter();
                        newNodesIdsMap[nodeId] = newNodeId.toString();
                        oldNodesIdsMap[newNodesIdsMap[nodeId]] = nodeId;

                        nodesTree[newNodeId] = created[nodeId];
                        nodesTree[newNodeId].id = newNodeId.toString();

                        if (nodesTree[newNodeId].parent && newNodesIdsMap[nodesTree[newNodeId].parent]) {
                            nodesTree[newNodeId].parent = newNodesIdsMap[nodesTree[newNodeId].parent];
                        }

                        modifiedNodes[nodeId] = nodesTree[newNodeId];
                    }
                }
                if (modified) {
                    for (var nodeId in modified) {
                        nodesTree[nodeId] = modified[nodeId];
                    }
                }

                if (deleted) {
                    for (var nodeId in deleted) {
                        nodesTree[nodeId] = deleted[nodeId];
                        this.deleteAllChildren(nodeId, nodesTree, modifiedNodes, oldNodesIdsMap, modified, notChanged);
                    }
                }

                this.localStorageService.set(config.common.storage.tree, nodesTree);
                deferred.resolve(modifiedNodes);

            }, 300);

            return deferred.promise;
        }
    }

}

interface ITreeService {
    getNode(id: string): ng.IPromise<schemas.INode>;
    getTree(): ng.IPromise<schemas.INodes>;
    saveChanges(created: schemas.INodes,
                modified: schemas.INodes,
                deleted: schemas.INodes,
                notChanged: schemas.INodes): ng.IPromise<schemas.INodes>;
    resetTree(): ng.IPromise<boolean>;
}

export = ITreeService;