/**
 * Created by DmitrijTrifonov on 02/06/2015.
 */
import config = require("../../config");
import angular = require("angular");

angular.module(config.appName).factory(config.common.srv.auth, getAuthService);

getAuthService.$inject = ["$http", "$q","localStorageService"];

function getAuthService($http: ng.IHttpService, $q: ng.IQService,localStorageService: ng.local.storage.ILocalStorageService) {
    return new AuthService($http, $q, localStorageService);
}

interface IUser {
    id: string;
    email: string;
    password: string;
    name: string;
    role: string;
}

class AuthService implements IAuthService {
    private currentUser: IUser;

    private loggedOn: boolean = false;

    constructor(private $http:ng.IHttpService,
                private $q:ng.IQService,
                private localStorageService: ng.local.storage.ILocalStorageService) {

        var user: IUser = <IUser>localStorageService.get(config.common.storage.authUser);
        this.predefineUsers();
        if (user) {
            this.currentUser = user;
            this.loggedOn = true;
        } else {
            this.currentUser = null;
            this.loggedOn = false;
        }
    }

    private predefineUsers() {
        var userManager = {
            id: "1",
            email: "manager@ankar.com",
            password: "123",
            name: "John Dohn",
            role: "Manager"
        };
        var userHR = {
            id: "2",
            email: "hr@ankar.com",
            password: "123",
            name: "June Willis",
            role: "HR"
        };
        this.localStorageService.set(config.common.storage.userPrefix+userManager.email,userManager);
        this.localStorageService.set(config.common.storage.userPrefix+userHR.email,userHR);
    }


    logoff() {
        var defer : ng.IDeferred<boolean> = this.$q.defer();
        this.loggedOn = false;
        this.currentUser = null;
        this.localStorageService.remove(config.common.storage.authUser);
        defer.resolve(true);

        return defer.promise;
    }

    logon(email:string) {
        var defer : ng.IDeferred<boolean> = this.$q.defer();

        var user: IUser = <IUser>this.localStorageService.get(config.common.storage.userPrefix+email);

        if (user) {
            this.currentUser = user;
            this.loggedOn = this.localStorageService.set(config.common.storage.authUser,user);
            defer.resolve(true);
        } else {
            defer.resolve(false);
        }
        return defer.promise;
    }

    isLoggedOn() {
        return this.loggedOn;
    }

    getCurrentUserId() {
        if (!this.currentUser) return null;
        return this.currentUser.id;
    }

    getCurrentUserName() {
        if (!this.currentUser) return null;
        return this.currentUser.name;
    }

    getCurrentUserRole() {
        if (!this.currentUser) return null;
        return this.currentUser.role;
    }
}

interface IAuthService {
    logon(email: string): ng.IPromise<boolean>;
    logoff(): ng.IPromise<boolean>;
    getCurrentUserId(): string;
    getCurrentUserName(): string;
    getCurrentUserRole(): string;
    isLoggedOn(): boolean;
}

export = IAuthService;