/**
 * Created by DmitrijTrifonov on 03/09/2015.
 */
import config = require("../../config");
import schemas = require("../schemas");
import ITreeService = require("./tree");
import angular = require("angular");

angular.module(config.appName).factory(config.common.srv.treeCache, getTreeCacheService);

getTreeCacheService.$inject = ["$http", "$timeout", "$q", "localStorageService", config.common.srv.tree];

function getTreeCacheService($http:ng.IHttpService,
                             $timeout:ng.ITimeoutService,
                             $q:ng.IQService,
                             localStorageService:ng.local.storage.ILocalStorageService,
                             treeService:ITreeService) {
    return new TreeCacheService($http, $timeout, $q, localStorageService, treeService);
}

class TreeCacheService implements ITreeCacheService {

    private counter:number = 0;

    constructor(private $http:ng.IHttpService,
                private $timeout:ng.ITimeoutService,
                private $q:ng.IQService,
                private localStorageService:ng.local.storage.ILocalStorageService,
                private treeService:ITreeService) {

        if (!localStorageService.get(config.common.storage.treeCachePrefixCounter)) {
            localStorageService.set(config.common.storage.treeCachePrefixCounter, this.counter);
        } else {
            this.counter = localStorageService.get<number>(config.common.storage.treeCachePrefixCounter);
        }

        if (!localStorageService.get<schemas.ILocalNodes>(config.common.storage.treeCache)) {
            localStorageService.set<schemas.ILocalNodes>(config.common.storage.treeCache, {});
        }
    }

    private getNextCounter():string {
        this.counter += 1;
        this.localStorageService.set(config.common.storage.treeCachePrefixCounter, this.counter);
        return config.common.storage.newPrefix + this.counter;
    }


    getTree():schemas.ILocalNodes {
        return this.localStorageService.get<schemas.ILocalNodes>(config.common.storage.treeCache);
    }

    getNode(id:string):ng.IPromise<schemas.ILocalNode> {
        var deferred = this.$q.defer();

        if (this.localStorageService.get(config.common.storage.treeCache)[id]) {
            deferred.resolve(this.localStorageService.get(config.common.storage.treeCache)[id]);
        } else {
            this.treeService.getNode(id).then((node) => {
                this.putNode(node);
                deferred.resolve(node);
            });
        }

        return deferred.promise;
    }

    createNode(parent:string):ng.IPromise<schemas.ILocalNode> {
        var deferred = this.$q.defer();
        var node = <schemas.ILocalNode>{id: this.getNextCounter(), parent: parent, value: "New item", deleted: false};
        this.$timeout(() => {
            this.putNode(node);
            deferred.resolve(node);
        }, 100);
        return deferred.promise;
    }

    private checkParentDeleted(tree:schemas.ILocalNodes, node:schemas.ILocalNode) {
        if (node.parent && tree[node.parent]) {
            return node.deleted = this.checkParentDeleted(tree,tree[node.parent]);
        } else {
            return node.deleted;
        }
    }

    private setChildrenDeleted(tree:schemas.ILocalNodes, node:schemas.ILocalNode) {
        for (var nodeId in tree) {
            if (tree[nodeId].parent == node.id) {
                tree[nodeId].deleted = true;
                this.setChildrenDeleted(tree,tree[nodeId]);
            }
        }
    }

    putNode(node:schemas.ILocalNode) {
        var tree = this.localStorageService.get<schemas.ILocalNodes>(config.common.storage.treeCache);
        tree[node.id] = node;
        if (this.checkParentDeleted(tree, tree[node.id])) {
            this.setChildrenDeleted(tree, tree[node.id])
        }
        this.localStorageService.set(config.common.storage.treeCache, tree);
    }

    saveChanges():ng.IPromise<boolean> {
        var deferred = this.$q.defer();
        var nodes:schemas.ILocalNodes = this.localStorageService.get<schemas.INodes>(config.common.storage.treeCache);
        var created:schemas.ILocalNodes = {};
        var deleted:schemas.ILocalNodes = {};
        var modified:schemas.ILocalNodes = {};
        var nonChanged:schemas.ILocalNodes = {};


        for (var nodeId in nodes) {
            if (nodes[nodeId].id.indexOf && nodes[nodeId].id.indexOf(config.common.storage.newPrefix) == 0) {
                if (nodes[nodeId].deleted) {
                    delete nodes[nodeId];
                } else {
                    created[nodeId] = nodes[nodeId];
                }
            } else if (nodes[nodeId].modified && nodes[nodeId].deleted) {
                deleted[nodeId] = nodes[nodeId];
            } else if (nodes[nodeId].modified) {
                modified[nodeId] = nodes[nodeId];
            } else {
                nonChanged[nodeId] = nodes[nodeId];
            }
        }

        this.treeService.saveChanges(created, modified, deleted, nonChanged).then((updatedNodes:schemas.INodes) => {

            for (var upNodeId in updatedNodes) {
                var upNode = updatedNodes[upNodeId];
                if (upNode.id != upNodeId) {
                    delete nodes[upNodeId];
                }

                nodes[upNode.id] = upNode;
            }

            for (var upNodeId in modified) {
                delete nodes[upNodeId]['modified'];
            }

            for (var upNodeId in deleted) {
                if (nodes[upNodeId]['modified']) {
                    delete nodes[upNodeId]['modified'];
                }
            }

            this.localStorageService.set(config.common.storage.treeCache, nodes);

            deferred.resolve(true);
        }, () => {
            deferred.resolve(false);
        });

        return deferred.promise;
    }

    resetCache():ng.IPromise<boolean> {
        var deferred = this.$q.defer();

        this.$timeout(() => {
            this.localStorageService.set(config.common.storage.treeCache, {});
            deferred.resolve(true);
        }, 300);

        return deferred.promise;
    }

}

interface ITreeCacheService {
    getTree():schemas.ILocalNodes;
    getNode(id:string): ng.IPromise<schemas.ILocalNode>;
    putNode(node:schemas.ILocalNode);
    saveChanges(): ng.IPromise<boolean>;
    resetCache(): ng.IPromise<boolean>;
    createNode(parent:string): ng.IPromise<schemas.ILocalNode>;
}

export = ITreeCacheService;