/**
 * Created by DmitrijTrifonov on 03/09/2015.
 */
var config = require("../../config");
var angular = require("angular");
angular.module(config.appName).factory(config.common.srv.tree, getTreeService);
getTreeService.$inject = ["$http", "$timeout", "$q", "localStorageService"];
function getTreeService($http, $timeout, $q, localStorageService) {
    return new TreeService($http, $timeout, $q, localStorageService);
}
var TreeService = (function () {
    function TreeService($http, $timeout, $q, localStorageService) {
        this.$http = $http;
        this.$timeout = $timeout;
        this.$q = $q;
        this.localStorageService = localStorageService;
        this.nodesCount = 0;
        this.useRemoteApi = true;
        if (!this.useRemoteApi) {
            if (!localStorageService.get(config.common.storage.treeCounter)) {
                localStorageService.set(config.common.storage.treeCounter, this.nodesCount);
            }
            else {
                this.nodesCount = localStorageService.get(config.common.storage.treeCounter);
            }
            if (!localStorageService.get(config.common.storage.tree)) {
                this.resetTree();
            }
        }
    }
    TreeService.prototype.getNextCounter = function () {
        this.nodesCount += 1;
        this.localStorageService.set(config.common.storage.treeCounter, this.nodesCount);
        return this.nodesCount;
    };
    TreeService.prototype.createTree = function () {
        var deferred = this.$q.defer();
        this.createChildNode(null, 0, 4, deferred);
        deferred.notify();
        return deferred.promise;
    };
    TreeService.prototype.createChildNode = function (id, level, maxLevel, deferred) {
        var newId = this.getNextCounter().toString();
        var newNode = {
            id: newId,
            parent: id,
            value: "Node " + newId,
            deleted: false
        };
        var nodes = this.localStorageService.get(config.common.storage.tree);
        nodes[newNode.id] = newNode;
        this.localStorageService.set(config.common.storage.tree, nodes);
        if (level < maxLevel) {
            this.createChildNode(newNode.id, level + 1, maxLevel, deferred);
            this.createChildNode(newNode.id, level + 1, maxLevel, deferred);
        }
        else {
            return deferred.resolve(true);
        }
    };
    TreeService.prototype.deleteAllChildren = function (parentId, tree, modifiedNodes, oldNodesIdsMap, modified, notChanged) {
        for (var id in tree) {
            if (tree[id].parent == parentId) {
                tree[id].deleted = true;
                if (oldNodesIdsMap && oldNodesIdsMap[id] && modifiedNodes && modifiedNodes[oldNodesIdsMap[id]]) {
                    modifiedNodes[oldNodesIdsMap[id]].deleted = true;
                }
                else if (modified && modified[id]) {
                    modified[id].deleted = true;
                    modifiedNodes[id] = modified[id];
                }
                else if (notChanged && notChanged[id]) {
                    notChanged[id].deleted = true;
                    modifiedNodes[id] = notChanged[id];
                }
                this.deleteAllChildren(id, tree, modifiedNodes, oldNodesIdsMap, modified, notChanged);
            }
        }
    };
    TreeService.prototype.resetTree = function () {
        var http = this.$http;
        if (this.useRemoteApi) {
            return http.post(config.apiUrl + "reset?format=json").then(function (callback) { return true; });
        }
        else {
            this.nodesCount = 0;
            var nodes = {};
            this.localStorageService.set(config.common.storage.treeCounter, this.nodesCount);
            this.localStorageService.set(config.common.storage.tree, nodes);
            return this.createTree();
        }
    };
    TreeService.prototype.getTree = function () {
        var _this = this;
        var http = this.$http;
        if (this.useRemoteApi) {
            return http.get(config.apiUrl + "nodes?format=json").then(function (callback) { return callback.data; });
        }
        else {
            var deferred = this.$q.defer();
            this.$timeout(function () {
                deferred.resolve(_this.localStorageService.get(config.common.storage.tree));
            }, 300);
            return deferred.promise;
        }
    };
    TreeService.prototype.getNode = function (id) {
        var _this = this;
        var http = this.$http;
        if (this.useRemoteApi) {
            return http.get(config.apiUrl + "node/" + id + "?format=json").then(function (callback) { return callback.data; });
        }
        else {
            var deferred = this.$q.defer();
            this.$timeout(function () {
                deferred.resolve(_this.localStorageService.get(config.common.storage.tree)[id]);
            }, 300);
            return deferred.promise;
        }
    };
    TreeService.prototype.saveChanges = function (created, modified, deleted, notChanged) {
        var _this = this;
        var http = this.$http;
        if (this.useRemoteApi) {
            return http.post(config.apiUrl + "apply?format=json", { created: created, modified: modified, deleted: deleted, notChanged: notChanged }).then(function (callback) { return callback.data; });
        }
        else {
            var deferred = this.$q.defer();
            this.$timeout(function () {
                var nodesTree = _this.localStorageService.get(config.common.storage.tree);
                var newNodesIdsMap = {};
                var oldNodesIdsMap = {};
                var modifiedNodes = {};
                if (created) {
                    for (var nodeId in created) {
                        var newNodeId = _this.getNextCounter();
                        newNodesIdsMap[nodeId] = newNodeId.toString();
                        oldNodesIdsMap[newNodesIdsMap[nodeId]] = nodeId;
                        nodesTree[newNodeId] = created[nodeId];
                        nodesTree[newNodeId].id = newNodeId.toString();
                        if (nodesTree[newNodeId].parent && newNodesIdsMap[nodesTree[newNodeId].parent]) {
                            nodesTree[newNodeId].parent = newNodesIdsMap[nodesTree[newNodeId].parent];
                        }
                        modifiedNodes[nodeId] = nodesTree[newNodeId];
                    }
                }
                if (modified) {
                    for (var nodeId in modified) {
                        nodesTree[nodeId] = modified[nodeId];
                    }
                }
                if (deleted) {
                    for (var nodeId in deleted) {
                        nodesTree[nodeId] = deleted[nodeId];
                        _this.deleteAllChildren(nodeId, nodesTree, modifiedNodes, oldNodesIdsMap, modified, notChanged);
                    }
                }
                _this.localStorageService.set(config.common.storage.tree, nodesTree);
                deferred.resolve(modifiedNodes);
            }, 300);
            return deferred.promise;
        }
    };
    return TreeService;
})();
//# sourceMappingURL=tree.js.map