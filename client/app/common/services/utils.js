/**
 * Created by DmitrijTrifonov on 03/09/2015.
 */
var config = require("../../config");
var angular = require("angular");
angular.module(config.appName).factory(config.common.srv.utils, getUtilsService);
getUtilsService.$inject = [];
function getUtilsService() {
    return new UtilsService();
}
var UtilsService = (function () {
    function UtilsService() {
    }
    UtilsService.prototype.findParentInNode = function (treeNode, parentId) {
        if (treeNode.children && treeNode.children[parentId]) {
            return treeNode.children[parentId];
        }
        else if (treeNode.children) {
            var result;
            for (var nodeId in treeNode.children) {
                result = this.findParentInNode(treeNode.children[nodeId], parentId);
                if (result)
                    break;
            }
            return result;
        }
        else {
            return undefined;
        }
    };
    UtilsService.prototype.findParent = function (tree, parentId) {
        if (parentId) {
            if (tree[parentId]) {
                return tree[parentId];
            }
            var parent;
            for (var nodeId in tree) {
                parent = this.findParentInNode(tree[nodeId], parentId);
                if (parent)
                    break;
            }
            return parent;
        }
        else {
            return undefined;
        }
    };
    UtilsService.prototype.findChildren = function (tree, node) {
        for (var nodeId in tree) {
            if (tree[nodeId].parent == node.id) {
                node.children[nodeId] = tree[nodeId];
            }
        }
        for (var child_id in node.children) {
            delete tree[child_id];
        }
    };
    UtilsService.prototype.nodesToTree = function (nodes) {
        var tree = {};
        for (var id in nodes) {
            var node = nodes[id];
            var parent = this.findParent(tree, node.parent);
            var treeViewNode = {
                id: id,
                parent: node.parent,
                value: node.value,
                deleted: node.deleted,
                children: {}
            };
            this.findChildren(tree, treeViewNode);
            if (parent) {
                parent.children[treeViewNode.id] = treeViewNode;
            }
            else {
                tree[treeViewNode.id] = treeViewNode;
            }
        }
        return tree;
    };
    UtilsService.prototype.treeNodeToLocalNode = function (treeNode) {
        var node = {
            id: treeNode.id,
            parent: treeNode.parent,
            value: treeNode.value,
            deleted: treeNode.deleted
        };
        if (treeNode['modified']) {
            node.modified = true;
        }
        return node;
    };
    return UtilsService;
})();
//# sourceMappingURL=utils.js.map