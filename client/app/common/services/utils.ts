/**
 * Created by DmitrijTrifonov on 03/09/2015.
 */
import config = require("../../config");
import angular = require("angular");
import schemas = require("../schemas");

angular.module(config.appName).factory(config.common.srv.utils, getUtilsService);

getUtilsService.$inject = [];

function getUtilsService() {
    return new UtilsService();
}

class UtilsService implements IUtilsService {

    private findParentInNode(treeNode: schemas.ITreeViewNode,parentId: string):schemas.ITreeViewNode {
        if (treeNode.children && treeNode.children[parentId]) {
            return treeNode.children[parentId]
        } else if (treeNode.children) {
            var result;
            for(var nodeId in treeNode.children) {
                result =  this.findParentInNode(treeNode.children[nodeId],parentId);
                if (result)  break;
            }
            return result;
        } else {
            return undefined;
        }
    }

    private findParent(tree: schemas.ITreeView, parentId: string):schemas.ITreeViewNode {
        if (parentId) {
            if (tree[parentId]) {
                return tree[parentId];
            }
            var parent;
            for (var nodeId in tree) {
                parent = this.findParentInNode(tree[nodeId],parentId);
                if (parent) break;
            }
            return parent;
        } else {
            return undefined;
        }
    }

    private findChildren(tree: schemas.ITreeView, node: schemas.ITreeViewNode ) {
        for (var nodeId in tree) {
            if (tree[nodeId].parent == node.id) {
                node.children[nodeId] = tree[nodeId]
            }
        }
        for (var child_id in node.children) {
            delete tree[child_id]
        }
    }

    nodesToTree(nodes: schemas.INodes):schemas.ITreeView {
        var tree: schemas.ITreeView = {};
        for (var id in nodes) {
            var node = nodes[id];

            var parent = this.findParent(tree,node.parent);

            var treeViewNode :schemas.ITreeViewNode = {
                id: id,
                parent: node.parent,
                value: node.value,
                deleted: node.deleted,
                children: {}
            };

            this.findChildren(tree,treeViewNode);

            if (parent) {
                parent.children[treeViewNode.id] = treeViewNode;
            } else {
                tree[treeViewNode.id] = treeViewNode;
            }
        }

        return tree;
    }

    treeNodeToLocalNode(treeNode:schemas.ITreeViewNode): schemas.ILocalNode {
        var node:schemas.ILocalNode = {
            id: treeNode.id,
            parent: treeNode.parent,
            value: treeNode.value,
            deleted: treeNode.deleted
        }
        if (treeNode['modified']) {
            node.modified = true;
        }
        return node;
    }
}

interface IUtilsService {
    nodesToTree(nodes: schemas.INodes):schemas.ITreeView;
    treeNodeToLocalNode(treeNode:schemas.ITreeViewNode): schemas.ILocalNode;
}

export = IUtilsService;