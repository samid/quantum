/**
 * Created by DmitrijTrifonov on 02/06/2015.
 */
var config = require("../../config");
var angular = require("angular");
angular.module(config.appName).factory(config.common.srv.auth, getAuthService);
getAuthService.$inject = ["$http", "$q", "localStorageService"];
function getAuthService($http, $q, localStorageService) {
    return new AuthService($http, $q, localStorageService);
}
var AuthService = (function () {
    function AuthService($http, $q, localStorageService) {
        this.$http = $http;
        this.$q = $q;
        this.localStorageService = localStorageService;
        this.loggedOn = false;
        var user = localStorageService.get(config.common.storage.authUser);
        this.predefineUsers();
        if (user) {
            this.currentUser = user;
            this.loggedOn = true;
        }
        else {
            this.currentUser = null;
            this.loggedOn = false;
        }
    }
    AuthService.prototype.predefineUsers = function () {
        var userManager = {
            id: "1",
            email: "manager@ankar.com",
            password: "123",
            name: "John Dohn",
            role: "Manager"
        };
        var userHR = {
            id: "2",
            email: "hr@ankar.com",
            password: "123",
            name: "June Willis",
            role: "HR"
        };
        this.localStorageService.set(config.common.storage.userPrefix + userManager.email, userManager);
        this.localStorageService.set(config.common.storage.userPrefix + userHR.email, userHR);
    };
    AuthService.prototype.logoff = function () {
        var defer = this.$q.defer();
        this.loggedOn = false;
        this.currentUser = null;
        this.localStorageService.remove(config.common.storage.authUser);
        defer.resolve(true);
        return defer.promise;
    };
    AuthService.prototype.logon = function (email) {
        var defer = this.$q.defer();
        var user = this.localStorageService.get(config.common.storage.userPrefix + email);
        if (user) {
            this.currentUser = user;
            this.loggedOn = this.localStorageService.set(config.common.storage.authUser, user);
            defer.resolve(true);
        }
        else {
            defer.resolve(false);
        }
        return defer.promise;
    };
    AuthService.prototype.isLoggedOn = function () {
        return this.loggedOn;
    };
    AuthService.prototype.getCurrentUserId = function () {
        if (!this.currentUser)
            return null;
        return this.currentUser.id;
    };
    AuthService.prototype.getCurrentUserName = function () {
        if (!this.currentUser)
            return null;
        return this.currentUser.name;
    };
    AuthService.prototype.getCurrentUserRole = function () {
        if (!this.currentUser)
            return null;
        return this.currentUser.role;
    };
    return AuthService;
})();
//# sourceMappingURL=auth.js.map