/**
 * Created by DmitrijTrifonov on 03/09/2015.
 */
var config = require("../../config");
var angular = require("angular");
angular.module(config.appName).factory(config.common.srv.treeCache, getTreeCacheService);
getTreeCacheService.$inject = ["$http", "$timeout", "$q", "localStorageService", config.common.srv.tree];
function getTreeCacheService($http, $timeout, $q, localStorageService, treeService) {
    return new TreeCacheService($http, $timeout, $q, localStorageService, treeService);
}
var TreeCacheService = (function () {
    function TreeCacheService($http, $timeout, $q, localStorageService, treeService) {
        this.$http = $http;
        this.$timeout = $timeout;
        this.$q = $q;
        this.localStorageService = localStorageService;
        this.treeService = treeService;
        this.counter = 0;
        if (!localStorageService.get(config.common.storage.treeCachePrefixCounter)) {
            localStorageService.set(config.common.storage.treeCachePrefixCounter, this.counter);
        }
        else {
            this.counter = localStorageService.get(config.common.storage.treeCachePrefixCounter);
        }
        if (!localStorageService.get(config.common.storage.treeCache)) {
            localStorageService.set(config.common.storage.treeCache, {});
        }
    }
    TreeCacheService.prototype.getNextCounter = function () {
        this.counter += 1;
        this.localStorageService.set(config.common.storage.treeCachePrefixCounter, this.counter);
        return config.common.storage.newPrefix + this.counter;
    };
    TreeCacheService.prototype.getTree = function () {
        return this.localStorageService.get(config.common.storage.treeCache);
    };
    TreeCacheService.prototype.getNode = function (id) {
        var _this = this;
        var deferred = this.$q.defer();
        if (this.localStorageService.get(config.common.storage.treeCache)[id]) {
            deferred.resolve(this.localStorageService.get(config.common.storage.treeCache)[id]);
        }
        else {
            this.treeService.getNode(id).then(function (node) {
                _this.putNode(node);
                deferred.resolve(node);
            });
        }
        return deferred.promise;
    };
    TreeCacheService.prototype.createNode = function (parent) {
        var _this = this;
        var deferred = this.$q.defer();
        var node = { id: this.getNextCounter(), parent: parent, value: "New item", deleted: false };
        this.$timeout(function () {
            _this.putNode(node);
            deferred.resolve(node);
        }, 100);
        return deferred.promise;
    };
    TreeCacheService.prototype.checkParentDeleted = function (tree, node) {
        if (node.parent && tree[node.parent]) {
            return node.deleted = this.checkParentDeleted(tree, tree[node.parent]);
        }
        else {
            return node.deleted;
        }
    };
    TreeCacheService.prototype.setChildrenDeleted = function (tree, node) {
        for (var nodeId in tree) {
            if (tree[nodeId].parent == node.id) {
                tree[nodeId].deleted = true;
                this.setChildrenDeleted(tree, tree[nodeId]);
            }
        }
    };
    TreeCacheService.prototype.putNode = function (node) {
        var tree = this.localStorageService.get(config.common.storage.treeCache);
        tree[node.id] = node;
        if (this.checkParentDeleted(tree, tree[node.id])) {
            this.setChildrenDeleted(tree, tree[node.id]);
        }
        this.localStorageService.set(config.common.storage.treeCache, tree);
    };
    TreeCacheService.prototype.saveChanges = function () {
        var _this = this;
        var deferred = this.$q.defer();
        var nodes = this.localStorageService.get(config.common.storage.treeCache);
        var created = {};
        var deleted = {};
        var modified = {};
        var nonChanged = {};
        for (var nodeId in nodes) {
            if (nodes[nodeId].id.indexOf && nodes[nodeId].id.indexOf(config.common.storage.newPrefix) == 0) {
                if (nodes[nodeId].deleted) {
                    delete nodes[nodeId];
                }
                else {
                    created[nodeId] = nodes[nodeId];
                }
            }
            else if (nodes[nodeId].modified && nodes[nodeId].deleted) {
                deleted[nodeId] = nodes[nodeId];
            }
            else if (nodes[nodeId].modified) {
                modified[nodeId] = nodes[nodeId];
            }
            else {
                nonChanged[nodeId] = nodes[nodeId];
            }
        }
        this.treeService.saveChanges(created, modified, deleted, nonChanged).then(function (updatedNodes) {
            for (var upNodeId in updatedNodes) {
                var upNode = updatedNodes[upNodeId];
                if (upNode.id != upNodeId) {
                    delete nodes[upNodeId];
                }
                nodes[upNode.id] = upNode;
            }
            for (var upNodeId in modified) {
                delete nodes[upNodeId]['modified'];
            }
            for (var upNodeId in deleted) {
                if (nodes[upNodeId]['modified']) {
                    delete nodes[upNodeId]['modified'];
                }
            }
            _this.localStorageService.set(config.common.storage.treeCache, nodes);
            deferred.resolve(true);
        }, function () {
            deferred.resolve(false);
        });
        return deferred.promise;
    };
    TreeCacheService.prototype.resetCache = function () {
        var _this = this;
        var deferred = this.$q.defer();
        this.$timeout(function () {
            _this.localStorageService.set(config.common.storage.treeCache, {});
            deferred.resolve(true);
        }, 300);
        return deferred.promise;
    };
    return TreeCacheService;
})();
//# sourceMappingURL=tree-cache.js.map