/**
 * Created by DmitrijTrifonov on 03/09/2015.
 */


export interface INode {
    id: string;
    parent: string;
    deleted: boolean;
    value: string;
}

export interface INodes {
    [id:string]:INode;
}

export interface ILocalNodes {
    [id:string]:ILocalNode;
}

export interface ILocalNode extends INode{
    modified?:boolean;
}

export interface ITreeViewNodes {
    [id:string]: ITreeViewNode;
}

export interface ITreeViewNode extends INode {
    children: ITreeViewNodes;
}

export interface ITreeView {
    [id:string]: ITreeViewNode;
}