/// <reference path="../../typings/client.d.ts" />
'use strict';
var config = {
    appName: "quantumTreeWeb",
    common: {
        storage: {
            tree: "tree",
            treeCounter: "tree-counter",
            treeCache: "tree-cache",
            treeCachePrefixCounter: "tree-cache-prefix-counter",
            newPrefix: "new-"
        },
        srv: {
            utils: "utilsService",
            tree: "treeService",
            treeCache: "treeCacheService"
        }
    },
    sections: {
        root: "RootController"
    },
    templateUrl: function (url) {
        return url + "?b=3";
    },
    apiUrl: window.location.protocol + '//' + window.location.hostname + (window.location.port ? ':' + window.location.port : '') + "/api/"
};
module.exports = config;
//# sourceMappingURL=config.js.map