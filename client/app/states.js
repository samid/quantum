/// <reference path="../../typings/client.d.ts" />
'use strict';
var config = require("./config");
function configureStates($stateProvider, $urlRouterProvider) {
    $stateProvider.state("index", {
        url: "/index",
        templateUrl: config.templateUrl("sections/root.html"),
        controller: config.sections.root,
        controllerAs: "vm"
    });
    $urlRouterProvider.when("", "/index");
    $urlRouterProvider.when("/", "/index");
    $urlRouterProvider.otherwise("/index");
}
module.exports = configureStates;
//# sourceMappingURL=states.js.map