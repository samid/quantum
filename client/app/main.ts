/// <reference path="../../typings/client.d.ts" />
'use strict';
import config = require("./config");
import configureStates = require("./states");
import angular = require("angular");

var app: any = angular.module(config.appName,[
    require("ui.router"),
    require("angular.local.storage")
]);

require("./common/services/utils");
require("./common/services/tree");
require("./common/services/tree-cache");
require("./sections/root");

function configApp($stateProvider, $urlRouterProvider,localStorageServiceProvider,$httpProvider) {

    $httpProvider.defaults.xsrfCookieName = 'csrftoken';
    $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';

    localStorageServiceProvider.setPrefix(config.appName).setStorageType("localStorage").setNotify(true, true);

    configureStates($stateProvider, $urlRouterProvider);
}

configApp.$inject = ["$stateProvider", "$urlRouterProvider","localStorageServiceProvider","$httpProvider"];

app.config(configApp);
app.init = () => {};

export = app;
