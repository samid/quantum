/// <reference path="../../typings/client.d.ts" />
'use strict';
import config = require("./config");

function configureStates($stateProvider: ng.ui.IStateProvider, $urlRouterProvider: ng.ui.IUrlRouterProvider) {
    $stateProvider.state("index", {
        url: "/index",
        templateUrl: config.templateUrl("sections/root.html"),
        controller: config.sections.root,
        controllerAs: "vm"
    });

    $urlRouterProvider.when("", "/index");
    $urlRouterProvider.when("/", "/index");
    $urlRouterProvider.otherwise("/index");
}

export = configureStates;