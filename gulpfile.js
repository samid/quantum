/**
 * Created by DmitrijTrifonov on 03/06/2015.
 */
var gulp = require("gulp");
var browserify = require('browserify');
var source = require("vinyl-source-stream");
var buffer = require('vinyl-buffer');
var watchify = require('watchify');
var livereload = require('gulp-livereload');
var gulpif = require('gulp-if');
var minifyCss = require('gulp-minify-css');
var concatCss = require('gulp-concat-css');
var sourcemaps = require('gulp-sourcemaps');
var rename = require('gulp-rename');
var uglify = require('gulp-uglify');
//var streamify = require('gulp-streamify')
var gutil = require('gulp-util');
var watchApp;

gulp.task('minify-js-app-dev', function () {
    watchApp = false;
    return browserifyShareApp('./client/app/',true);
});

gulp.task('minify-js-app-release', function () {
    watchApp = false;
    return browserifyShareApp('./client/app/', false);
});

gulp.task('minify-css-app-dev', function () {
    return bundleCss("./client/app/css/", true);
});

gulp.task('minify-css-app-release', function () {
    return bundleCss("./client/app/css/", false);
});

gulp.task('watch-app', ['minify-js-app-dev'], function () {

    gulp.watch('./client/app/css/*.css',  ['minify-css-app-dev']);

    // Start live reload server
    livereload.listen(35729);
});

gulp.task('build-app', ['minify-js-app-release', 'minify-css-app-release'], function () {

});

function browserifyShareApp(dir,dev) {
    var b = browserify({
        cache: {},
        packageCache: {},
        fullPaths: false,
        debug: dev
    });

    if (watchApp) {
        // if watch is enable, wrap this bundle inside watchify
        b = watchify(b);
        b.on('update', function () {
            bundleShareApp(b, dir, dev);
        });
    }

    b.add(dir+'definitions.js');
    return bundleShareApp(b, dir, dev);
}

function bundleShareApp(b, dir, dev) {
    if (dev) {
        return b.bundle()
            .pipe(source('app.min.js'))
            .pipe(gulp.dest(dir))
            .pipe(gulpif(watchApp, livereload()));
    } else {
        return b.bundle()
            .pipe(source('app.js'))
            .pipe(buffer())
            .pipe(uglify())
            .on('error', gutil.log)
            .pipe(rename('app.min.js'))
            .pipe(gulp.dest(dir))
            .pipe(gulpif(watchApp, livereload()));
    }
}

function bundleCss(dir, dev) {
    if (dev) {
        return gulp.src(dir + 'all.css')
            .pipe(concatCss(dir + "bundle.min.css", {rebaseUrls:false}))
            .pipe(gulp.dest("."))
    } else {
        return gulp.src(dir + '**/*.css')
            .pipe(concatCss(dir + "all.css", { rebaseUrls: false }))
            .pipe(rename('bundle.css'))
            .pipe(gulp.dest(dir))
            .pipe(minifyCss({ rebase:false, relativeTo: dir }))
            .on('error', gutil.log)
            .pipe(rename('bundle.min.css'))
            .pipe(gulp.dest(dir));
    }
}