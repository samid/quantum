/// <reference path="./typings/tsd.d.ts" />
function getEnv(env) {
    switch (env) {
        case 'dev':
        default:
            return {
                authCookieName: "auth",
                authSecret: "dev-secret - no secret at all!",
                logsDir: "./logs/",
                listenPort: 8082
            };
    }
}
exports.getEnv = getEnv;
//# sourceMappingURL=env.js.map